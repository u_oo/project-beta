import React, { useEffect, useState} from 'react';

function ManufacturersList() {
    const [manuList, setManuList] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManuList(data.manufacturers);

        }

    }
    useEffect(() => {
        fetchData();
    }, []);

    return (

        <>
        <h1>Manufacturers</h1>
        <table className="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
        </tr>
        </thead>
        <tbody>
        {manuList.map(manufacturer => {
        return (
            <tr key={manufacturer.href}>
            <td>{manufacturer.name}</td>
            </tr>
        );
        })}
        </tbody>
        </table>
        </>

    )


}


export default ManufacturersList

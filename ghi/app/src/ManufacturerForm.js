import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";

function ManufacturerForm() {
    const navigate=useNavigate();
    const [manuName, setManu] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.name = manuName;

        const manuURL = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
          };

        const response = await fetch(manuURL, fetchConfig);
        if (response.ok) {
            const newManu = await response.json();

            setManu('');
            navigate('/manufacturers');

        }
    }

    const handleManuChange =(event) => {
        const value = event.target.value;
        setManu(value);
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-manufacturer-form">
              <div className="form-floating mb-3">
                <input onChange={handleManuChange} value={manuName} placeholder="Manufacturer Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Manufacturer Name</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
        </div>
    )
}

export default ManufacturerForm;

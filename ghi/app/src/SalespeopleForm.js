import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";

function SalespeopleForm() {
  const navigate=useNavigate();
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeID, setEmpID] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {}
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employeeID;

    const salesPeopleURL = 'http://localhost:8090/api/salespeople/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      },
    };

    const response = await fetch(salesPeopleURL, fetchConfig);
    if (response.ok) {
      const newSalesPerson = await response.json();

      setFirstName('');
      setLastName('');
      setEmpID('');
      navigate('/salespeople')
    }
  }
  

  const handleNameChange =(event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleLastNameChange =(event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleEmpIDChange =(event) => {
    const value = event.target.value;
    setEmpID(value);
  }



  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new Salesperson</h1>
        <form onSubmit={handleSubmit} id="create-salesperson-form">
          <div className="form-floating mb-3">
            <input onChange={handleNameChange} value={firstName} placeholder="First name" required type="text" name="first_name" id="first_name" className="form-control"/>
            <label htmlFor="first_name">First Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleLastNameChange} value={lastName} placeholder="Last name" required type="text" name="last_name" id="last_name" className="form-control"/>
            <label htmlFor="last_name">Last Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleEmpIDChange} value={employeeID} placeholder="Create employee ID" required type="text" name="employee_id" id="employee_id" className="form-control"/>
            <label htmlFor="employee_id">Employee ID</label>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
    </div>
)
}

export default SalespeopleForm

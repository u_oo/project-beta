import React, { useEffect, useState} from 'react';

function SalespeopleList() {
  const [salespersonList, setSalespersonList] = useState([]);

  const fetchData = async () => {
    const salespeopleURL = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(salespeopleURL);
    if (response.ok) {
      const data = await response.json();
      setSalespersonList(data.salespeople)
    }
  }
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
    <h1>Salespeople</h1>
    <table className="table table-striped">
    <thead>
    <tr>
        <th>Employee ID</th>
        <th>First Name</th>
        <th>Last Name</th>
    </tr>
    </thead>
    <tbody>
    {salespersonList.map(person => {
    return (
        <tr key={person.id}>
        <td>{person.employee_id}</td>
        <td>{person.first_name}</td>
        <td>{person.last_name}</td>
        </tr>
    );
    })}
    </tbody>
    </table>
    </>

  )

}

export default SalespeopleList

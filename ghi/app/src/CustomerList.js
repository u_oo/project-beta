import React, { useEffect, useState} from 'react';

function CustomerList() {
  const [customerList, setCustomerList] = useState([]);

  const fetchData = async () => {
    const customerURL = 'http://localhost:8090/api/customers/';
    const response = await fetch(customerURL);
    if (response.ok) {
      const data = await response.json();
      setCustomerList(data.customers)
    }
  }
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
    <h1>Customers</h1>
    <table className="table table-striped">
    <thead>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Phone Number</th>
        <th>Address</th>
    </tr>
    </thead>
    <tbody>
    {customerList.map(cust => {
    return (
        <tr key={cust.id}>
        <td>{cust.first_name}</td>
        <td>{cust.last_name}</td>
        <td>{cust.phone_number}</td>
        <td>{cust.address}</td>
        </tr>
    );
    })}
    </tbody>
    </table>
    </>

  )
}

export default CustomerList

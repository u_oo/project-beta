import { useState, useEffect } from 'react';

export default function VehicleModelsList() {
  const [vehicles, setVehicles] = useState([]);

  const fetchData = async() => {
    const response = await fetch ("http://localhost:8100/api/models");
    
    if (response.ok) {
      const data = await response.json();
      setVehicles(data.models);
    }
  }

  useEffect(() => {
    fetchData()
  }, [])


  return (
    <>
      <h1 className="h1-padded">Models</h1>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Manufacturer</th>
            <th scope="col">Picture</th>
          </tr>
        </thead>
        <tbody>
          {vehicles.map((vehicle, i) => {
            return (
              <tr key={vehicle.id}>
                <td scope="col">{ vehicle.name }</td>
                <td scope="col">{ vehicle.manufacturer.name }</td>
                <td scope="col">
                  <img 
                    src={ vehicle.picture_url } 
                    width="100px"
                  />
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </>
  )
}
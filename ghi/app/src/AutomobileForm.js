import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";

function AutomobileForm() {
    const navigate=useNavigate();
    const [AutoColor, setAutoColor] = useState('');
    const [autoyear, setAutoYear] = useState('');
    const [AutoVin, setAutoVin] = useState('');
    const [models, setModels] = useState([]);
    const [model, setModel] =useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.color = AutoColor;
        data.year = autoyear;
        data.vin = AutoVin;
        data.model_id = model;

        const autoURL = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
          };

        const response = await fetch(autoURL, fetchConfig);
        if (response.ok) {
            const newAuto = await response.json();

            setAutoColor('');
            setAutoYear('');
            setAutoVin('');
            setModel('');
            navigate('/automobiles');

        }
    }
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setModels(data.models);
        }
      }

    const handleAutoColorChange =(event) => {
        const value = event.target.value;
        setAutoColor(value);
    }

    const handleAutoYearChange =(event) => {
        const value = event.target.value;
        setAutoYear(value);
    }

    const handleAutoVinChange =(event) => {
        const value = event.target.value;
        setAutoVin(value);

    }
    const handleModelChange =(event) => {
        const value = event.target.value;
        setModel(value);

    }
    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Automobile</h1>
            <form onSubmit={handleSubmit} id="create-manufacturer-form">
              <div className="form-floating mb-3">
                <input onChange={handleAutoColorChange} value={AutoColor} placeholder="Automobile color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
              <input placeholder="year" onChange={handleAutoYearChange} value={autoyear} required type="number" name="year" id="year" className="form-control"/>
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleAutoVinChange} value={AutoVin} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">Automobile VIN</label>
            </div>
            <div className="mb-3">
              <select onChange={handleModelChange} value={model} required name="model" id="model" className="form-select">
                <option value="">Choose a model</option>
                {models.map(model => {
                    return (
                        <option value={model.id} key={model.id}>
                            {model.name}
                        </option>

                    );
                  })}
              </select>
            </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
        </div>
    )
}

export default AutomobileForm;

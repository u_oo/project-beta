import React, { useEffect, useState} from 'react';

function SalesHistory() {
  const [salesList, setSalesList] = useState([]);
  const [salesPersonList, setSalesPeopleList] = useState([]);
  const [salesPerson, setSalesPerson] = useState('');
  const [filteredList, setFilteredList] = useState([]);

  const handleSalesPersonChange = (event) => {
    const value = event.target.value;
    setSalesPerson(value);
  }

  const fetchData = async () => {
    const salesURL = 'http://localhost:8090/api/sales/';
    const response = await fetch(salesURL);
    if (response.ok) {
      const data = await response.json();
      setSalesList(data.sales);
    }
  }

  const filterData = () => {
    const filtered = salesList.filter( sale => sale.salesperson.id == salesPerson);
    setFilteredList(filtered);
  }

  const SalesPeopleData = async () => {
    const salespeopleURL = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(salespeopleURL);
    if (response.ok) {
      const data = await response.json();
      setSalesPeopleList(data.salespeople)
    }

  }
  useEffect(() => {
    fetchData();
    SalesPeopleData();
  }, []);

  useEffect(() => {
    filterData()
  }, [salesPerson]);

  return (
    <>
    <h1>Salesperson History</h1>
    <div className="mb-3">
          <select onChange={handleSalesPersonChange} value={salesPerson} required name="salesperson" id="salesperson" className="form-select">
            <option value="">Choose a Salesperson</option>
            {salesPersonList.map(salesper => {
                return (
                    <option value={salesper.id} key={salesper.id}>
                        {salesper.first_name} {salesper.last_name}
                    </option>
                );
              })}
          </select>
        </div>
    <table className="table table-striped">
    <thead>
    <tr>
        <th>Salesperson</th>
        <th>Customer</th>
        <th>VIN</th>
        <th>Price</th>
    </tr>
    </thead>
    <tbody>
    {filteredList.map(sale => {
    return (
        <tr key={sale.id}>
        <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
        <td>{sale.customer.first_name} {sale.customer.last_name}</td>
        <td>{sale.automobile.vin}</td>
        <td>${sale.price}</td>
        </tr>
    );
    })}
    </tbody>
    </table>
    </>

  )
}


export default SalesHistory

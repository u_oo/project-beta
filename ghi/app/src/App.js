import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ManufacturersList from "./ManufacturersList";
import ManufacturerForm from "./ManufacturerForm";
import VehicleModelsList from "./VehicleModelsList";
import VehicleModelForm from "./VehicleModelForm";
import AutomobileList from "./AutomobileList";
import AutomobileForm from "./AutomobileForm";
import SalespeopleForm from "./SalespeopleForm";
import SalespeopleList from "./SalespeopleList";
import CustomerForm from "./CustomerForm";
import CustomerList from "./CustomerList";
import SalesList from "./SalesList"
import SalesHistory from "./SalesHistory";
import SalesForm from "./SalesForm";
import TechnicianList from "./TechnicianList";
import TechnicianForm from "./TechnicianForm";
import ServiceList from "./ServiceList";
import ServiceAppointmentForm from "./ServiceAppointmentForm";
import ServiceHistoryList from "./ServiceHistoryList";


export default function App(props) {
	return (
		<BrowserRouter>
			<Nav />
			<div className="container">
				<Routes>
					<Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
						<Route index element={<ManufacturersList />} />
						<Route path="new" element={<ManufacturerForm />} />
					</Route>
					<Route path="vehicles">
						<Route index element={<VehicleModelsList />} />
						<Route path="new" element={<VehicleModelForm />} />
					</Route>
          <Route path="automobiles">
						<Route index element={<AutomobileList />} />
						<Route path="new" element={<AutomobileForm />} />
					</Route>
          <Route path="salespeople">
						<Route index element={<SalespeopleList />} />
						<Route path="new" element={<SalespeopleForm />} />
					</Route>
          <Route path="customers">
            <Route index element={<CustomerList />} />
						<Route path="new" element={<CustomerForm />} />
					</Route>
          <Route path="sales">
						<Route index element={<SalesList/>} />
						<Route path="history" element={<SalesHistory />} />
						<Route path="new" element={<SalesForm />} />
          </Route>
          <Route path="technicians">
						<Route index element={<TechnicianList />} />
						<Route path="new" element={<TechnicianForm />} />
					</Route>
          <Route path="appointments">
						<Route index element={<ServiceList />} />
						<Route path="new" element={<ServiceAppointmentForm />} />
						<Route path="history" element={<ServiceHistoryList />} />
					</Route>
				</Routes>
			</div>
		</BrowserRouter>
	);
}

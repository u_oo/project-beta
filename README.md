# CarCar

Team:

- Nicole Kash - Sales Microservice
- Uch O - Service Microservice

## Design

CarCar is designed to manage inventory, sales, and service center for car dealerships.
CarCar includes seperate microservices for inventory, sales, and service and these microservices are integrated via polling. Each of the three microservices has RESTful APIs to manage data and communicate with the other microservices. This application uses Docker containers. There are seperate containers for the React front end, database, inventory API, service API, sales API,
service poller, and sales poller.

![CarCar Diagram](ghi/app/public/carcar_diagram.png "CarCar Diagram")

## How to run CarCar

1. Fork the project at https://gitlab.com/nicoleneeka10/project-beta and clone via HTTPS on your machine.
2. Run the following commands in your terminal in order:
   - docker volume create beta-data
   - docker-compose build
   - docker-compose up
3. Make sure all containers are running and allow time for the React server to start.
4. Navigate to http://localhost:3000/

## Inventory

The Inventory Microservice allows car dealerships to better tracke their inventory. The inventory microservice has three models listed below. All models include an automatically assigned database id, referred to as “id”, in addition to the attributes described below. The inventory microservice is integrated with the sales microservice. Automobiles added to the inventory in the inventory microservice are sent to the sales microservice via polling. To add an automobile to the inventory, you must first create a manufacturer. To create a manufacturer, you must first create a model.

### Inventory Microservice Models

1. Manufacturer
   - name (CharField)
2. VehicleModel
   - name (CharField)
   - picture_url (URLField)
   - manufacturer (ForeignKey)
3. Automobile
   - color (CharField)
   - year (PositiveSmallIntegerField)
   - vin (CharField)
   - sold (BooleanField)
   - model (ForeignKey)

## Inventory Microservice URLS, Routes, and CRUD Routes (Port 8100)

### Manufacturer

| Action                         | Method | URL                                          |
| ------------------------------ | ------ | -------------------------------------------- |
| List manufacturers             | GET    | http://localhost:8100/api/manufacturers/     |
| Create a manufacturer          | POST   | http://localhost:8100/api/manufacturers/     |
| Get a specific manufacturer    | GET    | http://localhost:8100/api/manufacturers/:id/ |
| Update a specific manufacturer | PUT    | http://localhost:8100/api/manufacturers/:id/ |
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/:id/ |

**Create a manufacturer:**

POST at http://localhost:8100/api/manufacturers/

Example JSON for POST:

```
{
  "name": "Tesla"
}
```

The response after successfully creating a new manufacturer includes an additional attribute “id” which is automatically created by the database and used for identification purposes throughout the Inventory Microservice. A href is also automatically created.

Example response:

```
{
	"href": "/api/manufacturers/3/",
	"id": 3,
	"name": "Tesla"
}
```

**List manufacturers:**

GET at http://localhost:8100/api/manufacturers/

Example response:

```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		},
		{
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Jeep"
		},
		{
			"href": "/api/manufacturers/3/",
			"id": 3,
			"name": "Tesla"
		},
		{
			"href": "/api/manufacturers/4/",
			"id": 4,
			"name": "Toyota"
		}
	]
}
```

**Specific manufacturer details:**

Use the database assigned "id" to get the details of a specific manufacturer

GET at http://localhost:8100/api/manufacturers/:id/

Example response:

```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Chrysler"
}
```

**Update a specific manufacturer:**

Use the database assigned "id" to update a specific manufacturer. Updating a manufacturer only requires the name field to be changed.

PUT at http://localhost:8100/api/manufacturers/:id/

Example JSON for PUT:

```
{
  "name": "Chryslerrrr"
}
```

Example response:

```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Chryslerrrr"
}
```

**Delete a manufacturer:**

Use the database assigned id to delete a manufacturer.

DELETE at http://localhost:8100/api/manufacturers/:id/

The response after successfully deleting a manufacturer is below.

```
{
	"id": null,
	"name": "ZoomZoom"
}
```

### Vehicle Model

| Action                          | Method | URL                                   |
| ------------------------------- | ------ | ------------------------------------- |
| List vehicle models             | GET    | http://localhost:8100/api/models/     |
| Create a vehicle model          | POST   | http://localhost:8100/api/models/     |
| Get a specific vehicle model    | GET    | http://localhost:8100/api/models/:id/ |
| Update a specific vehicle model | PUT    | http://localhost:8100/api/models/:id/ |
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/:id/ |

**Create a model:**

POST at http://localhost:8100/api/models/

Example JSON for POST:

```
{
  "name": "R8",
  "picture_url": "https://hips.hearstapps.com/hmg-prod/images/2023-audi-r8-gt-front-three-quarters-motion-3-1664827965.jpg?crop=0.684xw:0.577xh;0.0321xw,0.281xh&resize=1200:*",
  "manufacturer_id": 6
}

```

The response after successfully creating a new model includes an additional attribute “id” which is automatically created by the database and used for identification purposes throughout the Inventory Microservice. An href is also automatically created.

Example response:

```
{
	"href": "/api/models/2/",
	"id": 2,
	"name": "R8",
	"picture_url": "https://hips.hearstapps.com/hmg-prod/images/2023-audi-r8-gt-front-three-quarters-motion-3-1664827965.jpg?crop=0.684xw:0.577xh;0.0321xw,0.281xh&resize=1200:*",
	"manufacturer": {
		"href": "/api/manufacturers/6/",
		"id": 6,
		"name": "Audi"
	}
}
```

**List models:**

GET at http://localhost:8100/api/models/

Example response:

```
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Sebring",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Chrysler"
			}
		},
		{
			"href": "/api/models/2/",
			"id": 2,
			"name": "R8",
			"picture_url": "https://hips.hearstapps.com/hmg-prod/images/2023-audi-r8-gt-front-three-quarters-motion-3-1664827965.jpg?crop=0.684xw:0.577xh;0.0321xw,0.281xh&resize=1200:*",
			"manufacturer": {
				"href": "/api/manufacturers/6/",
				"id": 6,
				"name": "Audi"
			}
		}
	]
}
```

**Specific model details:**

Use the database assigned "id" to get the details of a specific model.

GET at http://localhost:8100/api/models/:id/

```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chryslerrrr"
	}
}
```

**Update a specific model:**

Use the database assigned "id" to update a specific model. Updating a model includes updating the name and/or picture URL. You cannot update the model's manufacturer.

http://localhost:8100/api/models/:id/

Example JSON for PUT:

```
{
  "name": "Sebringgg"
}
```

Example response:

```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebringgg",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chryslerrrr"
	}
}
```

**Delete a specific model:**
Use the database assigned id to delete a model.

DELETE at http://localhost:8100/api/models/:id/

The response after successfully deleting a model is below.

```
{
	"id": null,
	"name": "R800000",
	"picture_url": "https://hips.hearstapps.com/hmg-prod/images/2023-audi-r8-gt-front-three-quarters-motion-3-1664827965.jpg?crop=0.684xw:0.577xh;0.0321xw,0.281xh&resize=1200:*",
	"manufacturer": {
		"href": "/api/manufacturers/6/",
		"id": 6,
		"name": "Audi"
	}
}
```

### Automobile

| Action                       | Method | URL                                         |
| ---------------------------- | ------ | ------------------------------------------- |
| List automobiles             | GET    | http://localhost:8100/api/automobiles/      |
| Create an automobile         | POST   | http://localhost:8100/api/automobiles/      |
| Get a specific automobile    | GET    | http://localhost:8100/api/automobiles/:vin/ |
| Update a specific automobile | PUT    | http://localhost:8100/api/automobiles/:vin/ |
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/:vin/ |

**Create an automobile:**

http://localhost:8100/api/automobiles/

Use the vehicle model's database id in the "model_id" field.

Example JSON for POST:

```
{
  "color": "red",
  "year": 2012,
  "vin": "sssssssssssssssss",
  "model_id": 1
}

```

The response after successfully creating a new automobile includes an additional attribute “id” which is automatically created by the database and used for identification purposes throughout the Inventory Microservice. A href is also automatically created.

Example response:

```
{
	"href": "/api/automobiles/sssssssssssssssss/",
	"id": 9,
	"color": "red",
	"year": 2012,
	"vin": "sssssssssssssssss",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
```

**List automobiles:**

GET at http://localhost:8100/api/automobiles/

Example response:

```
{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN120174/",
			"id": 1,
			"color": "red",
			"year": 2012,
			"vin": "1C3CC5FB2AN120174",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Chrysler"
				}
			},
			"sold": false
		},
		{
			"href": "/api/automobiles/FFFFC5FB2AN12FFFF/",
			"id": 2,
			"color": "White",
			"year": 2023,
			"vin": "FFFFC5FB2AN12FFFF",
			"model": {
				"href": "/api/models/2/",
				"id": 2,
				"name": "R8",
				"picture_url": "https://hips.hearstapps.com/hmg-prod/images/2023-audi-r8-gt-front-three-quarters-motion-3-1664827965.jpg?crop=0.684xw:0.577xh;0.0321xw,0.281xh&resize=1200:*",
				"manufacturer": {
					"href": "/api/manufacturers/6/",
					"id": 6,
					"name": "Audi"
				}
			},
			"sold": false
		}
	]
}
```

**Specific automobile details:**

Use automobile vin get the details of a specific automobile.

GET at http://localhost:8100/api/automobiles/:vin/

Example response:

```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebringgg",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chryslerrrr"
		}
	},
	"sold": false
}
```

**Update a specific automobile:**

Use the vin to get a specific automobile to update. You can only update the color, year, and sold status of an automobile.

PUT at http://localhost:8100/api/automobiles/:vin/

Example JSON for PUT:

```
{
  "color": "blue"
}
```

Example response:

```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "blue",
	"year": 2012,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebringgg",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chryslerrrr"
		}
	},
	"sold": false
}
```

**Delete an automobile:**

Use the vin to delete an automobile.

DELETE at http://localhost:8100/api/automobiles/:vin/

The response after successfully deleting a automobile is below.

```
{
	"href": "/api/automobiles/sdf5hdfs5gdsdfghr/",
	"id": null,
	"color": "red",
	"year": 2012,
	"vin": "sdf5hdfs5gdsdfghr",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebringgg",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chryslerrrr"
		}
	},
	"sold": false
}
```

## Service Microservice

The Service Microservice is where car dealership staff can mantain records of service appointments, technicians, and customers. The service microservice has three models listed below. All models include an automatically assigned database id, referred to as “id”, in addition to the attributes described below. The Service Microservice is integrated with the Inventory Microservice. The AutomobileVO is a value object created by polling the Inventory Microservice. AutomobileVOs are uniquely identified via VIN number. With the service appointments filtering functionality, users are able to quickly look up current appointments using the VIN number.

### [](#service-microservice-models)Service Microservice Models

1.  AutomobileVO (value object)
    - vin (CharField)
    - import_href (BooleanField)
2.  Technician
    - first_name (CharField)
    - last_name (CharField)
    - employee_id (CharField)
3.  Appointment
    - vin (CharField)
    - technician (ForeignKey)
    - date_time (DateTimeField)
    - reason (CharField)
    - customer (CharField)
    - is_vip (CharField)

## [](#service-microservice-urls-routes-and-crud-routes-port-8080)Service Microservice URLS, Routes, and CRUD Routes (Port 8080)

### [](#technician)Technician

Action

Method

URL

List technicians

GET

[http://localhost:8080/api/technicians/](http://localhost:8080/api/technicians/)

Create a technician

POST

[http://localhost:8080/api/technicians/](http://localhost:8080/api/technicians/)

Delete a technician

DELETE

[http://localhost:8080/api/technicians/:id](http://localhost:8080/api/technicians/:id)

**Create a technician:**

POST at [http://localhost:8080/api/technicians/](http://localhost:8080/api/technicians/)

Example JSON for POST:

```
{
  "first_name": "Mike",
  "last_name": "Colte",
  "employee_id": "mcolte"
}
```

The response after successfully creating a new technician includes an additional attribute “id” which is automatically created by the database and used for identification purposes throughout the Service Microservice.

Example response:

```
{
  "first_name": "Mike",
  "last_name": "Colte",
  "employee_id": "mcolte"
  "id": 23
}
```

**Delete a technician:**

Use the database assigned “id” to delete a technician, **NOT** the employee_id.

DELETE at [http://localhost:8080/api/technicians/:id](http://localhost:8080/api/technicians/:id)

The response after successfully deleting a technician is below.

```
{
	"deleted": true
}
```

**List technicians:**

GET at [http://localhost:8080/api/technicians/](http://localhost:8080/api/technicians/)

Example response:

```
{
	"technicians": [
		{
			"first_name": "Mike",
			"last_name": "Colte",
			"employee_id": "mcolte",
			"id": 1
		},
		// ...
	]
}
```

### [](#appointments)Appointments

Action

Method

URL

List appointments

GET

[http://localhost:8080/api/appointments/](http://localhost:8080/api/appointments/)

Create an appointment

POST

[http://localhost:8080/api/appointments/](http://localhost:8080/api/appointments/)

Delete an appointment

DELETE

[http://localhost:8080/api/appointments/id/](http://localhost:8080/api/appointments/:id)

**Create an appointment:**

The status will be set to "Started" but default.

POST at [http://localhost:8080/api/appointments/](http://localhost:8080/api/appointments/)

Example JSON for POST:

```
{
	"vin": "1GCFG25F6V1059733",
	"customer": "Nicole Elderton",
	"date_time": "2023-04-30T10:45",
	"technician": 1,
	"reason": "Oil change"
}
```

The response after successfully creating a new appointment includes an additional attribute “id” which is automatically created by the database and used for identification purposes throughout the Service Microservice. The VIP field is automatically populated by checking the Inventory API to see if the vin matches the VIN of any automobiles sold. The technician's full information (first name, last name, employee id and attribute “id” is also provided with each appointment.

Example response:

```
{
	"appointment": {
		"href": "/api/appointments/20",
		"vin": "1GCFG25F6V1059733",
		"status": "Created",
		"technician": {
			"first_name": "Mike",
			"last_name": "Colte",
			"employee_id": "mcolte",
			"id": 1
		},
		"date_time": "2023-04-30T10:45",
		"reason": "Oil change",
		"customer": "Nicole Elderton",
		"is_vip": false,
		"id": 20
	}
}
```

**Delete an appointment:**

Use the database assigned id to delete an appointment.

DELETE at [http://localhost:8080/api/appointments/:id](http://localhost:8080/api/appointments/:id)

The response after successfully deleting an appointment is below.

```
{
	"deleted": true
}
```

**Update appointment status to canceled:**

Use the database assigned id to update an appointment's status to canceled.

PUT at [http://localhost:8080/api/appointments/:id/cancel](http://localhost:8080/api/appointments/:id/cancel)

The response after successfully deleting an appointment is below. The status reflects what the status shows up as in the database.

```
{
	"action": "cancel",
	"appointment": {
		"href": "/api/appointments/2",
		"vin": "1GCFG25F6V1059733",
		"status": "Canceled",
		"technician": {
			"first_name": "Mike",
			"last_name": "Colte",
			"employee_id": "mcolte",
			"id": 1
		},
		"date_time": "2023-04-30T10:45:00+00:00",
		"reason": "Windshield",
		"customer": "Nicole Elderton",
		"is_vip": true,
		"id": 2
	}
}
```

**Update appointment status to finished:**

Use the database assigned id to update an appointment's status to finished.

PUT at [http://localhost:8080/api/appointments/:id/finish](http://localhost:8080/api/appointments/:id/finish)

The response after successfully updating an appointment is below. The action field communicates what the term at the end of the link is. The status reflects what the status shows up as in the database.

```
{
	"action": "finish",
	"appointment": {
		"href": "/api/appointments/2",
		"vin": "1GCFG25F6V1059733",
		"status": "Finished",
		"technician": {
			"first_name": "Mike",
			"last_name": "Colte",
			"employee_id": "mcolte",
			"id": 1
		},
		"date_time": "2023-04-30T10:45:00+00:00",
		"reason": "Windshield",
		"customer": "Nicole Elderton",
		"is_vip": true,
		"id": 2
	}
}
```

**List appointments**

GET at [http://localhost:8080/api/appointments/](http://localhost:8080/api/appointments/)

Example response:

```
{
	"appointments": [
		{
			"href": "/api/appointments/2",
			"vin": "1GCFG25F6V1059733",
			"status": "Finished",
			"technician": {
				"first_name": "Mike",
				"last_name": "Colte",
				"employee_id": "mcolte",
				"id": 1
			},
			"date_time": "2023-04-30T10:45:00+00:00",
			"reason": "Windshield",
			"customer": "Nicole Elderton",
			"is_vip": true,
			"id": 2
		},
		//...
	]
}
```

## Sales Microservice

The Sales Microservice is where car dealership staff can easily keep track of sales, available automobiles, and customers. The sales microservice has four models listed below. All models include an automatically assigned database id, referred to as “id”, in addition to the attributes described below. The Sales Microservice is integrated with the Inventory Microservice. The AutomobileVO is a value object created by polling the Inventory Microservice. AutomobileVOs are identified via vin number and have the additional boolean property to identify if it has been sold. Once an AutomobileVO is sold, the value of sold changes to true and the AutomobileVO is no longer available for sale unless the sale is later deleted due to the buyer having cold feet.

### Sales Microservice Models

1. AutomobileVO (value object)
   - vin (CharField)
   - sold (BooleanField)
2. SalesPerson
   - first_name (CharField)
   - last_name (CharField)
   - employee_id (CharField)
3. Customer
   - first_name (CharField)
   - last_name (CharField)
   - address (CharField)
   - phone_number (PositiveBigIntegerField)
4. Sale
   - automobile (ForeignKey)
   - salesperson (ForeignKey)
   - customer (ForeignKey)
   - price (DecimalField)

## Sales Microservice URLS, Routes, and CRUD Routes (Port 8090)

### Salesperson

| Action               | Method | URL                                       |
| -------------------- | ------ | ----------------------------------------- |
| List salespeople     | GET    | http://localhost:8090/api/salespeople/    |
| Create a salesperson | POST   | http://localhost:8090/api/salespeople/    |
| Delete a salesperson | DELETE | http://localhost:8090/api/salespeople/:id |

**Create a salesperson:**

POST at http://localhost:8090/api/salespeople/

Example JSON for POST:

```
{
  "first_name": "Joshy",
  "last_name": "Josh",
  "employee_id": "JJosh"
}
```

The response after successfully creating a new salesperson includes an additional attribute “id” which is automatically created by the database and used for identification purposes throughout the Sales Microservice.

Example response:

```
{
	"first_name": "Joshy",
	"last_name": "Josh",
	"employee_id": "JJosh",
	"id": 3
}
```

**Delete a salesperson:**

Use the database assigned “id” to delete a salesperson, **NOT** the employee_id.

DELETE at http://localhost:8090/api/salespeople/:id

The response after successfully deleting a salesperson is below.

```
{
	"message": "deleted"
}
```

**List salespeople:**

GET at http://localhost:8090/api/salespeople/

Example response:

```
{
	"salespeople": [
		{
			"first_name": "Jenny",
			"last_name": "Jones",
			"employee_id": "JJones",
			"id": 1
		},
		{
			"first_name": "Evan",
			"last_name": "Hass",
			"employee_id": "EHass",
			"id": 4
		},
		{
			"first_name": "Selina",
			"last_name": "Salt",
			"employee_id": "SSalt",
			"id": 5
		}
	]
}
```

### Customer

| Action            | Method | URL                                     |
| ----------------- | ------ | --------------------------------------- |
| List customers    | GET    | http://localhost:8090/api/customers/    |
| Create a customer | POST   | http://localhost:8090/api/customers/    |
| Delete a customer | DELETE | http://localhost:8090/api/customers/:id |

**Create a customer:**

POST at http://localhost:8090/api/customers/

Example JSON for POST:

```
{
  "first_name": "Billy",
  "last_name": "Bob",
  "address": "2345 Blue St, Seattle, WA 98032",
   "phone_number": 4253459876
}
```

The response after successfully creating a new customer includes an additional attribute “id” which is automatically created by the database and used for identification purposes throughout the Sales Microservice.

Example response:

```
{
	"first_name": "Billy",
	"last_name": "Bob",
	"address": "2345 Blue St, Seattle, WA 98032",
	"phone_number": 4253459876,
	"id": 4
}
```

**Delete a customer:**

Use the database assigned id to delete a customer.

DELETE at http://localhost:8090/api/customers/:id

The response after successfully deleting a customer is below.

```
{
	"message": "deleted"
}
```

**List customers**

GET at http://localhost:8090/api/customers/

Example response:

```
{
	"customers": [
		{
			"first_name": "Patty",
			"last_name": "Pat",
			"address": "123 Main St, Seattle, WA 98032",
			"phone_number": 4257771234,
			"id": 1
		},
		{
			"first_name": "Billy",
			"last_name": "Bob",
			"address": "2345 Blue St, Seattle, WA 98032",
			"phone_number": 4253459876,
			"id": 3
		},
		{
			"first_name": "Katya",
			"last_name": "Katanya",
			"address": "6534 Maple Ln, Seattle, WA 98033",
			"phone_number": 4254444444,
			"id": 5
		}
	]
}
```

### Sales

| Action        | Method | URL                                 |
| ------------- | ------ | ----------------------------------- |
| List sales    | GET    | http://localhost:8090/api/sales/    |
| Create a sale | POST   | http://localhost:8090/api/sales/    |
| Delete a sale | DELETE | http://localhost:8090/api/sales/:id |

**Create a sale:**

POST at http://localhost:8090/api/sales/

Use salesperson database “id” numbers and customer database “id” numbers. Use the AutomobileVO’s vin number.

Example JSON for POST:

```
{
  "automobile": "NNNNNNNNNNNNNNNNN",
  "salesperson": 1,
  "customer": 3,
  "price": 10000.95
}
```

The response after successfully creating a new sale includes an additional attribute “id” which is automatically created by the database and used for identification purposes throughout the Sales Microservice. The response will also include all of the automobile, customer, and salesperson information. The automobile “sold” status will change to true and it will no longer be available for sale unless the sale is deleted.

Example response:

```
{
	"price": 10000.95,
	"id": 16,
	"automobile": {
		"vin": "NNNNNNNNNNNNNNNNN",
		"sold": true
	},
	"customer": {
		"first_name": "Billy",
		"last_name": "Bob",
		"address": "2345 Blue St, Seattle, WA 98032",
		"phone_number": 4253459876,
		"id": 3
	},
	"salesperson": {
		"first_name": "Jenny",
		"last_name": "Jones",
		"employee_id": "JJones",
		"id": 1
	}
}
```

**Delete a sale:**

Use the database assigned id to delete a sale.

DELETE at http://localhost:8090/api/sales/:id

The response after successfully deleting a sale below. Please note the AutomobileVO’s “sold” status will be changed back to true in the event the sale is deleted. In the event a buyer returns a vehicle due to cold feet, the vehicle will automatically be available for sale again.

```
{
	"message": "deleted"
}
```

**List sales:**

GET at http://localhost:8090/api/sales/

Example response:

```
{
	"sales": [

		{
			"price": "10000.95",
			"id": 13,
			"automobile": {
				"vin": "ABCABCVABCABCABCA",
				"sold": true
			},
			"customer": {
				"first_name": "Billy",
				"last_name": "Bob",
				"address": "2345 Blue St, Seattle, WA 98032",
				"phone_number": 4253459876,
				"id": 3
			},
			"salesperson": {
				"first_name": "Jenny",
				"last_name": "Jones",
				"employee_id": "JJones",
				"id": 1
			}
		},
		{
			"price": "10000.95",
			"id": 15,
			"automobile": {
				"vin": "sssssssssssssssss",
				"sold": true
			},
			"customer": {
				"first_name": "Billy",
				"last_name": "Bob",
				"address": "2345 Blue St, Seattle, WA 98032",
				"phone_number": 4253459876,
				"id": 3
			},
			"salesperson": {
				"first_name": "Jenny",
				"last_name": "Jones",
				"employee_id": "JJones",
				"id": 1
			}
		},
		{
			"price": "2000.00",
			"id": 19,
			"automobile": {
				"vin": "999999FB2AN777777",
				"sold": true
			},
			"customer": {
				"first_name": "Elena",
				"last_name": "Brown",
				"address": "4345 Orange Way, Kirkland, WA 90834",
				"phone_number": 4258756425,
				"id": 8
			},
			"salesperson": {
				"first_name": "Evan",
				"last_name": "Hass",
				"employee_id": "EHass",
				"id": 4
			}
		}
	]
}
```

### Available AutomobileVOs

| Action                       | Method | URL                                    |
| ---------------------------- | ------ | -------------------------------------- |
| List available AutomobileVOs | GET    | http://localhost:8090/api/automobiles/ |

**List available AutomobileVOs:**

GET at http://localhost:8090/api/automobiles/

Only AutomobileVOs with sold marked as “false” will be in this list.

Example response:

```
{
	"available_autos": [
		{
			"vin": "1C3CC5FB2AN120174",
			"sold": false
		},
		{
			"vin": "FFFFC5FB2AN12FFFF",
			"sold": false
		},
		{
			"vin": "333333FB2AN133333",
			"sold": false
		},
		{
			"vin": "999999FB2AN777777",
			"sold": false
		}
	]
}
```

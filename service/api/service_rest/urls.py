from django.urls import path, include
from .views import (
    technicians,
    technician,
    appointments,
    appointment
)


urlpatterns = [
    path(
      "technicians/", 
      technicians, 
      name="technicians"
    ),
    path(
      "technicians/<int:pk>",
      technician,
      name="technician"
    ),
    path(
      "appointments/", 
      appointments, 
      name="appointments"
    ),
    path(
      "appointments/<int:pk>",
      appointment,
      name="appointment"
    ),
    path(
      "appointments/<int:pk>/<str:action>",
      appointment,
      name="appointment"
    ),
    path(
      "appointments/<int:pk>/<str:action>",
      appointment,
      name="appointment"
    ),
]
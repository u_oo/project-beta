from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from .encoders import (
    TechnicianEncoder,
    TechnicianListEncoder, 
    AutomobileVOEncoder, 
    AppointmentEncoder)
from .models import Technician, AutomobileVO, Appointment

from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment

@require_http_methods(["GET", "POST"])
def technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)

            technician = Technician.objects.create(
                first_name=content["first_name"],
                last_name=content["last_name"],
                employee_id=content["employee_id"],
            )
            return JsonResponse(
                {"technician": technician},
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not add the technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def technician(request, pk):
    try:
        technician = Technician.objects.get(id=pk)
        count, _ = technician.delete()
        response = JsonResponse(
            {"deleted": count > 0}
        )
        return response
    except Technician.DoesNotExist:
        response = JsonResponse(
            {"message": "Technician does not exist"}
        )
        response.status_code = 404
        return response


@require_http_methods(["GET", "POST"])
def appointments(request, vin=None):
    if request.method == "GET":
        if vin is not None:
            appointments = Appointment.objects.filter(vin=vin)
        else:
            appointments = Appointment.objects.all()

        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder
        )
    else:
        try:
            content = json.loads(request.body)

            appointment = Appointment.objects.create(
                vin=content["vin"],
                technician=Technician.objects.get(id=content["technician"]),
                status=Appointment.Statuses.CREATED,
                customer=content["customer"],
                date_time=content["date_time"],
                reason=content["reason"],
            )
            return JsonResponse(
                {"appointment": appointment},
                encoder=AppointmentEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not add the appointment"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def appointment(request, pk, action=None):
    if request.method == "GET":
        appointments = Appointment.objects.get(id=pk)

        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False
        )
    
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            count, _ = appointment.delete()
            response = JsonResponse(
                {"deleted": count > 0}
            )
            return response
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "Appointment does not exist"}
            )
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body) if request.body else {}

            if action.lower() == "finish":
                content["status"]=Appointment.Statuses.FINISHED

            if action.lower() == "cancel":
                content["status"]=Appointment.Statuses.CANCELED
                
            Appointment.objects.filter(id=pk).update(status=content["status"])
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                {"action": action,
                 "appointment": appointment},
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Exception as e:
            response = JsonResponse(
                {"message": "Could not update the appointment",
                 "error": e}
            )
            response.status_code = 400
            return response
